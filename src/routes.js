import VueRouter from "vue-router";

import services from './components/services.vue'
import view from './components/viewService.vue'

export default new VueRouter({
    routes:[
        {
            name: 'index',
            path: '/',
            component: services
        },
        {
            name:'view',
            path: '/view',
            component: view
        },

    ]
})