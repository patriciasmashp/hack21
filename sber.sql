-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 21 2021 г., 12:05
-- Версия сервера: 10.3.22-MariaDB
-- Версия PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `sber`
--

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE `services` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `services`
--

INSERT INTO `services` (`ID`, `name`, `description`, `content`) VALUES
(1, 'Самокат', 'Nisi nostrud excepteur labore enim officia aliquip. Commodo reprehenderit eiusmod nulla tempor do id incididunt labore deserunt. Eiusmod ipsum ipsum amet quis est aliqua aute commodo aliquip velit eiusmod. Sunt nisi enim culpa sint Lorem occaecat do labore velit magna officia exercitation nisi nostrud. Quis magna sunt qui ex velit. Sunt amet et culpa voluptate deserunt.\r\n\r\nMollit nisi enim sunt ipsum amet Lorem incididunt sint laborum nostrud sit esse. Nisi ipsum labore tempor magna dolore exercitation irure do consectetur mollit consequat esse deserunt do. Anim pariatur amet eu qui anim ullamco consequat commodo aliquip ut incididunt. Elit culpa ad voluptate ex commodo ipsum mollit id. Officia duis ut deserunt duis fugiat in esse. Laboris dolor laborum esse sint est.\r\n\r\nLaboris minim fugiat cillum voluptate exercitation. Aliqua occaecat laboris do adipisicing adipisicing sit exercitation in aute eu aliquip. In consequat adipisicing consequat aute occaecat.', '<h2>TEEEEST</h2> <style>h2{color:red;}</style>'),
(4, 'Сбер доставка', 'asdasdasdasdasd', '<p>TEEEEEEST</p> <img src=\"https://rkoff.ru/wp-content/uploads/2019/05/rko_sberbank.png\">'),
(5, 'Lorem', 'dfsdfdsfs', '<script> console.log(\"sd\") </script>');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `services`
--
ALTER TABLE `services`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
