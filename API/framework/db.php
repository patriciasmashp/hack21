<? 
$config = require __DIR__ . '/config.php';

return $mysql = new mysqli($config['server'], $config['mysqlLogin'], $config['mysqlPass'], $config['database']);
