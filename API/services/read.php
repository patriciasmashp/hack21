<?
require __DIR__ . '/../framework/main.php';

$SQL = 'SELECT * FROM services';

$query = $db->query($SQL);

if (empty($db->error_list)) {

    while ($res = $query->fetch_assoc()) {
        
            $arRes[] = $res;                
    }
} else {

    foreach ($db->error_list as $key => $er) {

        print_r($er);
    }
}

echo (json_encode($arRes));